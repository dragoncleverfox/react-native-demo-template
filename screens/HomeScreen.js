import * as React from 'react';
import { View, Text, Button, StyleSheet, StatusBar, FlatList, Dimensions, Image, Animated, TouchableOpacity, Easing, SafeAreaViewBase, SafeAreaView, ImageBackground } from 'react-native';
import { useTheme } from '@react-navigation/native';
const { width, height } = Dimensions.get('screen');
import faker from 'faker';
import { LinearGradient } from 'expo-linear-gradient';


faker.seed(10);
const DATA = [...Array(30).keys()].map((_, i) => {
    return {
        key: faker.random.uuid(),
        image: `https://randomuser.me/api/portraits/${faker.helpers.randomize(['women', 'men'])}/${faker.random.number(60)}.jpg`,
        name: faker.name.findName(),
        jobTitle: faker.name.jobTitle(),
        email: faker.internet.email(),
    };
});

const SPACING = 20;
const AVATAR_SIZE = 70;
const BG_IMG = 'https://www.pexels.com/photo/pink-rose-closeup-photography-1231265/';
const HomeScreen = ({navigation}) => {

  const { colors } = useTheme();

  const theme = useTheme();
  // const scrollY = React.useRef(new Animated.Value(0)).current;
    return (

      <View style={styles.container}>
        {/* <StatusBar barStyle= { theme.dark ? "light-content" : "dark-content" }/> */}
        <StatusBar barStyle= {"light-content"}/>

        {/* <Text style={{color: colors.text}}>Home Screen</Text>
      <Button
        title="Go to details screen"
        onPress={() => navigation.navigate("Details")}
      /> */}
      <LinearGradient
                    colors={['#0d988d', '#ff00c6']}
      style={[styles.image]}
      >
      <FlatList 
      data={DATA}
      // onScroll={Animated.event([{ nativeEvent: {contentoffset: {y: scrollY}}}],
      // {useNativeDriver:true})}
      keyExtractor={item => item.key}
      contentContainerStyle={{
        padding:SPACING,
        paddingTop:StatusBar.currentHeight || 42
      }}
      renderItem={({item, index})=>{
        return <View style={[{flexDirection:'row', padding: SPACING, marginBottom:SPACING, backgroundColor:'rgba(225,225,225,0.8)', borderRadius:12 },styles.shadowBox]}>
          <Image 
          source={{uri:item.image}}
          style={{width:AVATAR_SIZE,height:AVATAR_SIZE, borderRadius:AVATAR_SIZE, marginRight:SPACING / 2 }}
          /> 
          <View>
            <Text style={{fontSize: 22,fontWeight:'700'}}>{item.name}</Text>
            <Text style={{fontSize: 18,opacity:.7,width:width / 1.7}}>{item.jobTitle}</Text>
            <Text style={{fontSize: 14,opacity:.8, color:'#0099cc',width:width / 1.7}}>{item.email}</Text>

          </View>
        </View>
      }}
      >

      </FlatList>
</LinearGradient>
      </View>
    );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    // alignItems: 'center', 
    // justifyContent: 'center'
    // backgroundColor:'#fff',
  },
  shadowBox: {
    shadowOffset: { width: 0, height: 10 },
    shadowColor: '#000',
    shadowOpacity: .3,
    shadowRadius:20,
    elevation: 20,
    // background color must be set
    backgroundColor : "white" // invisible color
  },
  image: {
    // flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
});
