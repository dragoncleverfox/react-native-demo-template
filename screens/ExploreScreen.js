import React, { useEffect, useState } from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';
import { AuthContext } from '../components/context';

const ExploreScreen = () => {
  const { signOut } = React.useContext(AuthContext);
    return (
      <View style={styles.container}>
        <Text>Sign Out</Text>
        <Button
          title="Click Here"
          onPress={() => {
            signOut();
          }}
        />
      </View>
    );
};

export default ExploreScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
});